package org.lokkie.libs.serialize

/**
 * Part of org.lokkie.libs.serialize. Created in lyaml
 *
 * @author lokkie
 * @version 0.1
 */
class SerializeException : Exception {
    constructor() : super()
    constructor(message: String?) : super(message)
    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(cause: Throwable?) : super(cause)
    constructor(message: String?, cause: Throwable?, enableSuppression: Boolean, writableStackTrace: Boolean) : super(message, cause, enableSuppression, writableStackTrace)
}