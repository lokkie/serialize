package org.lokkie.libs.serialize

import com.google.gson.GsonBuilder
import org.lokkie.gson.adapters.PartialJsonMapper
import org.lokkie.types.PartialMappedObject
import java.lang.reflect.Type

/**
 * Part of org.lokkie.libs.serialize. Created in serialize
 *
 * @author lokkie
 * @version 0.1
 */
class JsonSerializer : Serializer() {
    /**
     *
     */
    private val gsonBuilder: GsonBuilder = GsonBuilder().serializeNulls()
        // Register PartialMappedObject Adapter
        .registerTypeHierarchyAdapter(PartialMappedObject::class.java, PartialJsonMapper())

    /**
     * Registers Hierarchy type adapter (for interface adapting)
     *
     * @param clazz Interface to parse
     * @param typeAdapter adapter to parse with
     */
    fun registerTypeHierarchyAdapter(clazz: Class<*>, typeAdapter: Any): JsonSerializer {
        gsonBuilder.registerTypeHierarchyAdapter(clazz, typeAdapter)
        return this
    }

    /**
     * Registers type adapter (for classes adapting)
     *
     * @param type Type to parse
     * @param typeAdapter adapter to parse with
     */
    fun registerTypeAdapter(type: Type, typeAdapter: Any): JsonSerializer {
        gsonBuilder.registerTypeAdapter(type, typeAdapter).serializeNulls()
        return this
    }

    /**
     * Main deserialize method. Converts json string ([content]) into objects by [clazz] recommendation
     *
     * @param content String content to deserialize
     * @param clazz Deserialize instruction
     */
    @Throws(SerializeException::class)
    override fun <T> deserialize(content: String, clazz: Class<T>): T? = if (content.isEmpty())
        throw SerializeException("Empty data for config") else gsonBuilder.create().fromJson(content, clazz)

    /**
     * Main serialize method. Converts [Any] [data] into json string
     *
     * @param data Data to serialize into json
     */
    override fun serialize(data: Any?): String? = if (data == null) "{}" else gsonBuilder.create().toJson(data)

    /**
     * Enables lenient mode of Gson
     */
    fun setLenient(): JsonSerializer {
        gsonBuilder.setLenient()
        return this
    }

    companion object {
        /**
         * Converts 1 json per line into array
         *
         * @param incoming string to join
         */
        @JvmStatic
        fun normalizeJson(incoming: String): String = if (incoming.contains("}{"))
            "[" + incoming.replace("}{", "},{") + "]" else incoming
    }
}