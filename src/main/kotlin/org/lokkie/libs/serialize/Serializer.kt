package org.lokkie.libs.serialize

import java.io.InputStream

/**
 * Part of org.lokkie.libs.serialize. Created in lyaml
 *
 * @author lokkie
 * @version 0.1
 */
abstract class Serializer {
    @Throws(SerializeException::class)
    abstract fun <T> deserialize(content: String, clazz: Class<T>): T?

    @Throws(SerializeException::class)
    fun <T> deserialize(inputStream: InputStream, clazz: Class<T>): T? =
        deserialize(inputStream.bufferedReader().readText().trim(), clazz)

    @Throws(SerializeException::class)
    abstract fun serialize(data: Any?): String?
}