package org.lokkie.libs.serialize

import org.yaml.snakeyaml.DumperOptions
import org.yaml.snakeyaml.Yaml
import java.io.File
import java.io.FileNotFoundException

/**
 * Part of org.lokkie.libs.serialize. Created in serialize
 *
 * @author lokkie
 * @version 0.1
 */
class YamlSerializer : Serializer() {

    val yaml = Yaml(DumperOptions().apply {
        defaultFlowStyle = DumperOptions.FlowStyle.BLOCK
        defaultScalarStyle = DumperOptions.ScalarStyle.PLAIN
        isCanonical = false
    })

    @Throws(FileNotFoundException::class)
    private fun getIncludedContent(file: File, indent: String): String =
        file.readText().replace("\r\n", "\n").split("\n").joinToString("\n") { line ->
            "^[ \t]*@@include\\(([^)]+)\\);$".toRegex().run {
                if (matches(line)) {
                    val fileName = find(line)!!.groups[1]!!.value
                    getIncludedContent(
                        File((if (fileName.startsWith(".")) file.parent + File.separator else "") + fileName),
                        indent + ("^([ \t]+)[^ \t]*".toRegex()
                            .run { if (matches(line)) find(line)!!.groups[1]!!.value else "" }))
                } else indent + line
            }
        }

    @Throws(SerializeException::class)
    fun <T> deserialize(file: File, clazz: Class<T>): T? = deserialize(getIncludedContent(file, ""), clazz)


    @Throws(SerializeException::class)
    override fun <T> deserialize(content: String, clazz: Class<T>): T? = if (content.isEmpty())
        throw SerializeException("Empty data for config") else yaml.loadAs(content, clazz)

    @Throws(SerializeException::class)
    override fun serialize(data: Any?): String? = yaml.dump(data)
}